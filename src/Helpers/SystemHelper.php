<?php

namespace Grinderspro\Helpers;

/**
 * SystemHelper
 *
 * @author Grigoriy Miroschnichenko <grinderspro@gmail.com>
 */

class SystemHelper
{
    public static function removeBomUtf8($s)
    {
        if (substr($s, 0, 3) == chr(hexdec('EF')) . chr(hexdec('BB')) . chr(hexdec('BF'))) {
            return substr($s, 3);
        } else {
            return $s;
        }
    }

    public static function gen_uuid()
    {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            mt_rand(0, 0xffff), mt_rand(0, 0xffff),
            mt_rand(0, 0xffff),
            mt_rand(0, 0x0fff) | 0x4000,
            mt_rand(0, 0x3fff) | 0x8000,
            mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }

    /**
     * @param $replace
     * @param $subject - sql string
     * @return mixed
     */
    public static function joinReplace($replace, $subject)
    {
        $interval_string = str_replace('tovar.catalog_id', 'tc.catalog_id', $subject);
        return str_replace('/*{{JOIN}}*/', $replace, $interval_string);
    }

    /**
     * Модифицирует основной запрос [tovar_sql] под мультикат
     *
     * @param int $cat_id
     * @param $subject_string - sql string
     * @return mixed
     */
    public static function joinMultiCatReplace($cat_id = 0, $subject_string)
    {
        $interval_string = str_replace('tovar.catalog_id', 'tc.catalog_id', $subject_string);
        $join = "RIGHT JOIN tovar_to_catalog as tc ON tc.tovar_id = tovar.id AND tc.catalog_id=" . intval($cat_id);

        return str_replace('/*{{JOIN_MULTI_CAT}}*/', $join, $interval_string);
    }

    public static function formatBytes($size, $precision = 2)
    {
        $base = log($size, 1024);
        $suffixes = array('', 'Кб', 'Мб', 'Гб', 'Тб');

        return round(pow(1024, $base - floor($base)), $precision) .' '. $suffixes[floor($base)];
    }

    public static function float($digit){
        $digit = preg_replace('/[^0-9,\.\-]/', '', $digit);
        return doubleval(str_replace(',','.',$digit));
    }

    /**
     * @param $string
     * @param string $sec_string
     * @return mixed|null|string|string[]
     */
    public static function rus2translit($string, $sec_string='') {
        $converter = array(
            'а' => 'a',   'б' => 'b',   'в' => 'v',
            'г' => 'g',   'д' => 'd',   'е' => 'e',
            'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
            'и' => 'i',   'й' => 'y',   'к' => 'k',
            'л' => 'l',   'м' => 'm',   'н' => 'n',
            'о' => 'o',   'п' => 'p',   'р' => 'r',
            'с' => 's',   'т' => 't',   'у' => 'u',
            'ф' => 'f',   'х' => 'h',   'ц' => 'c',
            'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
            'ь' => '\'',  'ы' => 'y',   'ъ' => '\'',
            'э' => 'e',   'ю' => 'yu',  'я' => 'ya',

            'А' => 'A',   'Б' => 'B',   'В' => 'V',
            'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
            'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
            'И' => 'I',   'Й' => 'Y',   'К' => 'K',
            'Л' => 'L',   'М' => 'M',   'Н' => 'N',
            'О' => 'O',   'П' => 'P',   'Р' => 'R',
            'С' => 'S',   'Т' => 'T',   'У' => 'U',
            'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
            'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',
            'Ь' => '\'',  'Ы' => 'Y',   'Ъ' => '\'',
            'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
            '°' => '',    '±' => '',   '”' => '',
            '—' => '-',' ' => '-' ,'&'=>''
        ,'№' => '','»'=>'','«'=>'','“'=>'',','=>'',
            '’'=>'','…'=>'','"'=>'','®'=>''

        );

        $string =  strtr($string, $converter);
        $string = strtolower($string);
        $string = preg_replace('~[^-a-z0-9_]+~u', '', $string);
        $string = str_replace('_','-',$string);
        $string = trim($string, "-");
        return  $string;
    }
}
