<?php

namespace Grinderspro\Helpers;

/**
 * FileHelper
 *
 * @author Grinderspro <grinderspro@gmail.com>
 * @copyright Copyright (c) Compo 2018
 */

class FileHelper
{
    public static $mine_types = [
        // images
        'png' => 'image/png',
        'jpe' => 'image/jpeg',
        'jpeg' => 'image/jpeg',
        'jpg' => 'image/jpeg',
        'gif' => 'image/gif',
        'bmp' => 'image/bmp',
        'ico' => 'image/vnd.microsoft.icon',
        'tiff' => 'image/tiff',
        'tif' => 'image/tiff',
        'svg' => 'image/svg+xml',
        'svgz' => 'image/svg+xml',
    ];

    public static function delete()
    {

    }

    /**
     * @param $field - name $_FILES
     * @param string $uploads_dir
     * @return array
     * @throws \Exception
     */
    public static function loadFile($field, $uploads_dir = '')
    {
        if (empty($_FILES[$field]['tmp_name']))
            return false;

        if($uploads_dir == '')
            return false;

        $type = '';
        $fType = '';
        $filename = $_FILES[$field]['tmp_name'];
        $fileHeader = $_FILES[$field]['name'];
        $mimes_img = ['image/gif', 'image/jpeg', 'image/png'];
        $mimes = ['image/gif', 'image/jpeg', 'image/png', 'application/pdf', 'application/msword', 'application/vnd.ms-excel', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'];

        if ('picture' == $field) {
            if (!in_array($_FILES[$field]['type'], $mimes_img)) {
                throw new \Exception('Тип файла не поддерживается. Ожидается картинка: JPG, PNG, GIF');
            }
            if (($img_info = getimagesize($filename)) === false) {
                throw new \Exception('Тип файла не поддерживается. Ожидается картинка JPG, PNG, GIF');
            }
            if (!in_array($img_info[2], array(1, 2, 3))) {
                throw new \Exception('Тип файла не поддерживается. Ожидается картинка JPG, PNG, GIF');
            }
        } else {
            if (!in_array($_FILES[$field]['type'], $mimes)) {
                throw new \Exception('Тип файла не поддерживается. Ожидается: doc, docx, xls, xlsx, pdf, jpeg, png, gif');
            }
        }

        if(self::isImage($filename))
        {
            $type = 'Фото';
        } else {
            switch ($field) {
                case 'file_defect':
                    $type = 'Акт брака';
                    break;
                case 'file_upd':
                    $type = 'УПД';
                    break;
                default:
                    $type = 'Файл';
            }
        }

        switch ($_FILES[$field]['type']) {
            case 'image/gif':
                $fType = 'gif';
                break;
            case 'image/jpeg':
                $fType = 'jpg';
                break;
            case 'image/png':
                $fType = 'png';
                break;
            case 'application/pdf':
                $fType = 'pdf';
                break;
            case 'application/msword':
                $fType = 'doc';
                break;
            case 'application/vnd.ms-excel':
                $fType = 'xls';
                break;
            case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
                $fType = 'xlsx';
                break;
            case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
                $fType = 'docx';
                break;
        }

        // create dir
        if (!file_exists($uploads_dir)) {
            mkdir($uploads_dir, 0777, true);
        }

        $fSize = filesize($filename);
        $fName = $uploads_dir . DIRECTORY_SEPARATOR . md5_file($filename) . '.' . $fType;

        if (!file_exists($fName)) {
            $fp = fopen($fName, 'wb');
            fwrite($fp, fread(fopen($filename, "rb"), $fSize));
            fclose($fp);
            chmod($fName, 0666);
        }

        $file = [
            'type' => $type,
            'filename' => basename($fName),
            'header' => $fileHeader,
            'ftype' => $fType,
            'fsize' => $fSize
        ];

        return $file;
    }

    public static function isImage($path)
    {
        $image_type = getimagesize($path)['mime'];

        if(in_array($image_type, self::$mine_types))
            return true;

        return false;
    }
}
