<?php

namespace Grinderspro\Helpers;

/**
 * StringHelper
 *
 * @author Grigoriy Miroschnichenko <grinderspro@gmail.com>
 */

class StringHelper
{
    public static function makeUuid() {
        return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
            mt_rand( 0, 0xffff ),
            mt_rand( 0, 0x0fff ) | 0x4000,
            mt_rand( 0, 0x3fff ) | 0x8000,
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
        );
    }

    public static function strToEmail($string)
    {
        if (preg_match("/[a-zA-Z0-9]+[a-zA-Z0-9\._-]*@[a-zA-Z0-9_-]+(?:[a-zA-Z0-9\._-]+)+/is",$string,$match)) {
            return strtolower($match[0]);
        }
        return false;
    }

    public static function generatePassword($number=5)
    {
        $pass = "";
        $arr = ['1','2','3','4','5','6','7','8','9','0'];

        for($i = 0; $i < $number; $i++)
        {
            $index = rand(0, count($arr) - 1);
            $pass .= $arr[$index];
        }

        return $pass;
    }
}
