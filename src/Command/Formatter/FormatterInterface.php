<?php

namespace Compo\Command\Formatter;

interface FormatterInterface
{
    public function setConnector($connector);
    public function checkFormat($value, $format);
}