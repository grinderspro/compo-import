<?php

/**
 * Formatter
 *
 * @author Grinderspro <grinderspro@gmail.com>
 * @copyright Copyright (c) Compo 2019
 */

namespace Grinderspro\Command\Formatter;

use Grinderspro\Helpers\SystemHelper;
use Grinderspro\Models\Catalog;
use Grinderspro\Models\Customer;

class Formatter
{
    public $connector;
    public $product;
    public $catalog;
    public $brand;

    public function __construct($connector)
    {
        $this->connector = $connector;
        $this->product = new Product($this->connector);
        $this->catalog = new Catalog($this->connector);
        $this->brand = new Brand($this->connector);
    }

    public function checkFormat($value, $format, $params = [])
    {
        switch ($format) {
            case 'integer':
                return (integer)$value;
                break;
            case 'contract':
                return (new Contract($this->connector))->getIdByCode($value);
                break;
            case 'customer':
                return (new Customer($this->connector))->getByCode($value, 'id');
                break;
            case 'contragent':
                return (new Contragent($this->connector))->getIdByCode($value);
                break;
            case 'product_group':
                return (integer)$this->product->getGroup($value);
                break;
            case 'float':
                return SystemHelper::float($value);
                break;
            case 'string':
                return trim((string)$value);
                break;
            case 'date':
                return (string)date("Y-m-d", strtotime($value));
                break;
            case 'datetime':
                return (string)date("Y-m-d H:i:s", strtotime($value));
                break;
            case 'code1c':
                return SystemHelper::gen_uuid();
                break;
            case 'url':
                return SystemHelper::rus2translit($this->checkFormat($value, 'string')) . '_' . $params['next_id'] ? $params['next_id'] : $this->product->getLastId('tovar') . '.html';
                break;
            case 'brand':
                $brandId = $this->brand->getIdByHeader($value);
                return $brandId ? $brandId : $this->brand->setBrandByHeader($value);
                break;
        }

        return preg_replace('/\s+/iu', ' ', $value);
    }

    public function getSimpleInsertFields($hardFields)
    {
        $keys = [];

        if(empty($hardFields))
            return [];

        foreach ($hardFields as $field)
            $keys[] = key($field);

        return array_combine($keys, array_keys($hardFields));

    }
}