<?php

/**
 * ImportAbstract
 *
 * @author Grinderspro <grinderspro@gmail.com>
 * @copyright Copyright (c) Compo 2018
 */

namespace Grinderspro\Command;

use Grinderspro\Command\Formatter\Formatter;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

abstract class ImportAbstract extends Command
{
    public $table = '';
    public $formatter;
    public $connector;
    public $sql_insert_fields = [];
    public $sql_update_fields = [];
    public static $fields = [];
    protected $fields_insert = [];
    protected $fields_update = [];
    protected $fields_exclude = [];
    protected $fields_required = [];

    public function __construct()
    {
        parent::__construct();
        $this->formatter = new Formatter($this->connector);
        $this->makeFields();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->formatter = new Formatter($this->connector);
    }

    abstract protected function makeFields();

    abstract protected function sync($data);

    /**
     * Исполняет SQL запрос
     *
     * @param $sql - спрока запроса
     * @return integer|mixed
     */
    protected function executeSql($sql)
    {
        if(!$sql)
            return false;

        return $this->connector->sql->insert($sql);
    }

    /**
     * Возвращает готовый к исполнению SQL запрос
     *
     * @param $fields - insertFields список insert полей
     * @param string $table - table name, по умолчанию $this->table (дочернего класса)
     * @return string
     * @throws \Exception
     */
    protected function getSqlQueryString($fields, $table = '')
    {
        $table = $table == '' ? $this->table : (string)$table;

        if(!$table || empty($fields))
            throw new \Exception('Метод executeSql($params) - определите $this->table и $this->fields_insert');

        $insert_fields = $fields;
        $update_fields = $this->getNormalisedUpdateFields($this->getUpdateFields($insert_fields, $this->fields_exclude));

        if(!$this->isValidFields($insert_fields))
            return false;

        $SQL="INSERT INTO " . $table . "
                (" . implode (',', array_keys($insert_fields)) . ")
              VALUES
                (" . implode (',', array_map([$this->connector->sql,'real_escape'], array_values($insert_fields))) . ")
                ON DUPLICATE KEY UPDATE " . implode(',', $update_fields);

        return $SQL;
    }

    /**
     * Проверяет есть ли обязательные поля, которые не должны быть пустыми или 0 при импорте
     *
     * Например поле contract_id или contragent_id в  табл. credit_note (запись не имеет смысла без этих полей)
     *
     * @param $insert_fields
     * @return bool
     */
    private function isValidFields($insert_fields)
    {
        foreach ($this->fields_required as $field) {
            if(array_key_exists($field, $insert_fields) && !$insert_fields[$field]) {
                return false;
            }
        }
        return true;
    }

    /**
     * Вернет массив полей для INSERT
     *
     * @param $data - пучок данных из синх. файла (.json)
     * @param array $fields
     * @return array
     */
    protected function getInsertFields($data, $fields = [])
    {
        $total_fields = [];
        $_fields = $fields ? $fields : $this->fields_insert;

        foreach ($_fields as $key=>$value) {
            $val = $this->getValueFromData($data, $key);
            $total_fields[key($value)] = $this->formatter->checkFormat($val, current($value));
        }

        return $total_fields;
    }

    private function getValueFromData($data, $value)
    {
        $result = '';
        $dataIsObject = is_object($data);
        $valuePathArray = explode(".", $value);
        $count = count($valuePathArray);

        if($count == 1)
            $result = $dataIsObject ? $data->{$valuePathArray[0]} : $data[$valuePathArray[0]];
        elseif ($count == 2)
            $result = $dataIsObject ? $data->{$valuePathArray[0]}->{$valuePathArray[1]} : $data[$valuePathArray[0]][$valuePathArray[1]];
        elseif ($count == 3)
            $result = $dataIsObject ? $data->{$valuePathArray[0]}->{$valuePathArray[1]}->{$valuePathArray[2]} : $data[$valuePathArray[0]][$valuePathArray[1]][$valuePathArray[2]];

        return $result;
    }

    /**
     * Вертнет массив полей для UPDATE
     *
     * @param $fields_insert
     * @param array $fields_exclude - массив полей, которые не требается обновлять
     * @return array
     */
    protected function getUpdateFields($fields_insert, $fields_exclude = [])
    {
        $fld_update = [];
        $fld_exclude = $fields_exclude ? $fields_exclude : $this->fields_exclude;

        foreach ($fields_insert as $key=>$value)
        {
            if(!in_array($key, $fld_exclude))
                $fld_update[$key] = $value;
        }

        return $fld_update;
    }

    /**
     * Подготовит массив для ON DUPLICATE KEY UPDATE в формате field_name=value,....
     *
     * @param array $fields - массив полей для Update
     * @return array
     */
    protected function getNormalisedUpdateFields(array $fields = [])
    {
        $normal_fields = [];

        foreach ($fields as $key=>$value)
            $normal_fields[] = $key . "='" . $value . "'";

        return $normal_fields;
    }

    public function setConnector($connector)
    {
        $this->connector = $connector;
    }

}