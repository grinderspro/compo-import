<?php

/**
 * ImportTestCommand
 *
 * @author Grinderspro <grinderspro@gmail.com>
 * @copyright Copyright (c) Compo 2019
 */

namespace Grinderspro\Command;

use Grinderspro\Command\Reader\JsonReader;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ImportTestCommand extends ImportAbstract
{
    public $connector;
    public $table = '';
    private $file = 'sync/test.json';
    private $output;

    protected function configure()
    {
        $this->setName('compo:import-test')->setDescription('compo:import-test');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        parent::execute($input, $output);

        $this->output = $output;
        $this->table = 'credit_note';

        $json =  (new JsonReader())->setFile($this->file)->read('test');

        if(!$json)
            return false;

        foreach ($json as $data) {
            if(($obj = $this->sync($data)) === false)
                $this->output->writeln('[' . date("Y-m-d H:i:s") . '] "Not sync: ' . $data->UIDcontragent . '"');
        }

    }

    public function makeFields()
    {
        $this->fields_insert = [
            'name' => ['name' => 'string'],
            'customer' => ['customer_id' => 'customer'],
            'info.email' => ['email' => 'string'],
            'info.address.city' => ['city' => 'string']
        ];

        $this->fields_exclude = [];
    }

    protected function sync($data)
    {
        $id = 0;
        $insert_fields = $this->getInsertFields($data);

        try {
            $id = $this->executeSql($this->getSqlQueryString($insert_fields));
        } catch (\Exception $e) {
            echo 'Выброшено исключение: ', $e->getMessage(), "\n";
        }

        return $id ? $id : 0;
    }

}