<?php

/**
 * ReaderAbstract
 *
 * @author Grinderspro <grinderspro@gmail.com>
 * @copyright Copyright (c) Compo 2018
 */

namespace Grinderspro\Command\Reader;

abstract class ReaderAbstract
{
    protected $file;

    public function setFile($file)
    {
        $this->file = $file;
        return $this;
    }

    abstract public function read($node);

    protected function checkFormat($value, $format)
    {
    }

    protected function backupFile($file)
    {
    }
}