<?php

/**
 * XlsxReader
 *
 * @author Grinderspro <grinderspro@gmail.com>
 * @copyright Copyright (c) Compo 2018
 */

namespace Grinderspro\Command\Reader;

class XlsxReader extends ReaderAbstract
{
    public function read($node = '')
    {
        try {

            if(empty($this->file))
                throw new \Exception('Не задан $this->file. Выполните setFile() после создания экземпляра');
            if (!file_exists($this->file))
                throw new \Exception('File "' . $this->file . '" does not exist' . PHP_EOL);

            // Reader
            $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
            $reader->setReadDataOnly(true);
            $spreadsheet = $reader->load($this->file);
            $sheetData = $spreadsheet->getActiveSheet()->toArray();

            return $sheetData ? $sheetData : false;

        } catch (\Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }
}