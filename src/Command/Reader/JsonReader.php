<?php

/**
 * JsonReader
 *
 * @author Grinderspro <grinderspro@gmail.com>
 * @copyright Copyright (c) Compo 2018
 */

namespace Grinderspro\Command\Reader;

use Grinderspro\Helpers\SystemHelper;

class JsonReader extends ReaderAbstract
{
    public function read($node)
    {
        try {

            if (!file_exists($this->file)) throw new \Exception('File "' . $this->file . '" does not exist' . PHP_EOL);
            if (!$node) throw new \Exception('No node for reading' . PHP_EOL);

            echo '[' . date("Y-m-d H:i:s") . '] Read PROCCESSING FROM  ' . $this->file . PHP_EOL;

            $json = SystemHelper::removeBomUtf8(file_get_contents($this->file));
            $json = json_decode($json);

            return ($json && $json->{$node}) ? $json->{$node} : false;

        } catch (\Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }
}