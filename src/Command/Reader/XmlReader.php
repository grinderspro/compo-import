<?php

/**
 * XmlReader
 *
 * @author Grinderspro <grinderspro@gmail.com>
 * @copyright Copyright (c) Compo 2018
 */

namespace Grinderspro\Command\Reader;

use SimpleXMLElement;

class XmlReader extends ReaderAbstract
{
    public $reader;

    public function read($node = '')
    {
        try {

            if (!file_exists($this->file)) throw new \Exception('File "' . $this->file . '" does not exist');

            $this->reader = new \XMLReader();
            $this->reader->open($this->file);

            echo '[' . date("Y-m-d H:i:s") . '] "Begins parsing products"' . PHP_EOL;

            while ($this->reader->read()) {
                if($this->reader->nodeType == \XMLReader::ELEMENT && $this->reader->localName == $node) {
                    return new SimpleXMLElement($this->reader->readOuterXML());
                }
            }

        } catch (\Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }
}