<?php

namespace Grinderspro\Models;

use Compob2b\Core\Connector;

class Models
{

    protected $sql;
    protected $table;
    protected $connector;

    /**
     * Models constructor.
     * @param Connector $connector
     */
    function __construct(Connector $connector)
    {
        $this->table = static::TABLE; // использую позднее статическое связывание, для гарантий переопределения константы в дочерних классах. (else Fatal error)
        $this->connector = $connector; // Dependency injection
    }

    /**
     * Получение записи по ID
     *
     * В зависимости от  переопределения имени таблыцы БД в дочерних классах
     *
     * @param $id
     * @param string $field
     * @return array|bool|null
     */
    public function getById($id, $field = '')
    {
        $query = "SELECT c.* FROM " . $this->table . " as c WHERE c.id = '".(integer)$id."'";

        return $field ? $this->connector->sql->fetchRecord($query)[$field] : $this->connector->sql->fetchRecord($query);
    }

    /**
     * @param $code1c
     * @param string $field
     * @return array|bool|null
     */
    public function getByCode($code1c, $field = '')
    {
        $query = "SELECT o.* 
                  FROM " . $this->table . " as o
                  WHERE o.code1c = " . $this->connector->sql->real_escape($code1c);

        return $field ? $this->connector->sql->fetchRecord($query)[$field] : $this->connector->sql->fetchRecord($query);
    }
}