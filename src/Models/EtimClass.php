<?php

/**
 * EtimClass
 *
 * @author Grinderspro <grinderspro@gmail.com>
 * @copyright Copyright (c) Compo 2019
 */

namespace Grinderspro\Models;

class EtimClass
{
    const TABLE = 'etimartclass';

    public function getByCode($code, $field = '')
    {
        $query = "SELECT ec.* FROM " . self::DB_TABLE . " as ec WHERE ec.ARTCLASSID = '" . $code . "'";
        return $field ? $this->sql->fetchRecord($query)[$field] : $this->sql->fetchRecord($query);
    }
}