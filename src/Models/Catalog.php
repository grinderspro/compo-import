<?php

/**
 * Catalog
 *
 * @author Grinderspro <grinderspro@gmail.com>
 * @copyright Copyright (c) Compo 2019
 */

namespace Grinderspro\Models;

use Grinderspro\Helpers\SystemHelper;

class Catalog extends Models
{
    public function makeUrlByHeader($header, $next_id = 0)
    {
        $nextId = $next_id ? $next_id : $this->getLastId('catalog');
        return SystemHelper::rus2translit((string)$header) . '_' . $nextId . '.html';
    }

    public function getLastId($table)
    {
        return $this->sql->fetchRecord("SHOW TABLE STATUS WHERE `Name` = '" . $table . "'")['Auto_increment'];
    }

    public function getIdByEtimClass($etimClassCode)
    {
        return (new EtimClass($this->connector))->getByCode($etimClassCode, 'SECTIONID');
    }
}