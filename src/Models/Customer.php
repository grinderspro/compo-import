<?php

namespace Grinderspro\Models;

/**
 * Customer
 *
 * @author Grinderspro <grinderspro@gmail.com>
 * @copyright Copyright (c) Compo 2018
 */

class Customer extends Models
{
    const TABLE = 'customers';

    /**
     * @param $eventType - Тип события
     * @param int $customerId - Id кастомера. Если 0 - значит все.
     * @return mixed
     */
    public function getNotificationsEmails($eventType, $customerId = 0)
    {
        $whereCustomerId = $customerId ? ' AND c.id = ' . $customerId : '';
        $query = "SELECT
                    c.id,
                    c.email,
                    nd.type_id as disabled,
                    IF (COUNT(ne.email), CONCAT(CONCAT(c.email, ','), GROUP_CONCAT(ne.email)), c.email) as emails
                  FROM
                    ".self::DB_TABLE_TOVAR." as c
                  LEFT JOIN notifications_disabled as nd ON nd.customer_id = c.id AND nd.type_id = ".$eventType."
                  LEFT JOIN notifications_emails as ne ON ne.customer_id = c.id
                  WHERE
                    c.email != '' ".$whereCustomerId."
                  GROUP BY
                    c.id
                  HAVING
                    disabled IS NULL";

        return $this->connector->sql->fetchHash($query,1);
    }
}